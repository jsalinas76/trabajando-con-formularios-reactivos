import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  form: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.buildForm();
  }

  enviar(){
    console.log(this.form);

  }

  validacionPersonalizada(campo: AbstractControl): ValidationErrors | null{

    let numero = parseInt(campo.value);
    if(numero%2 == 0){
      return null;
    }
    return {'par': true};

  }

  validacionCadena(campo: AbstractControl): ValidationErrors | null{

    if(campo.value.startWith('A')){
      return null;
    }
    return {'principio': true};

  }

  private buildForm() {

    //let control1 = new FormControl('nombre');
    //let control2 = new FormControl('apellidos');

    /* Nombre del campo y valor por defecto
    this.form = this.formBuilder.group({
      email: '',
      password: '',

    });
    */
    this.form = this.formBuilder.group({
      email: ['contacto@cleformacion.com', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  reset(){
    this.form.reset({email: '', password: ''});
  }
}
